<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <H1> Buat Account Baru!</H1>

    <H4> Sign Up Form</H4>

    <form action="welcome" method="POST">
     @csrf
        <label> First name: </label> <br> <br>
            <input type="text" name="nama"> <br> <br>

        <label> Last name: </label> <br> <br>
            <input type="text" name="last"> <br> <br>

        <label> Gender: </label><br> <br>
            <input type="radio" name="gender"> Male<br>
            <input type="radio" name="gender"> Famale<br>
            <input type="radio" name="gender"> Other<br> <br>

        <label> Nationality: </label> <br> <br>
            <select name="Nationality">
                <option value="Indonesia"> Indonesia </option>
                <option value="Turkey"> Turkey </option>
                <option value="Rusia"> Rusia </option>
                <option value="US"> US </option>
            </select> <br> <br>
        
        <Label> Language Spoken: </Label> <br> <br>
            <input type="checkbox" name="language"> Bahasa Indonesia <br>
            <input type="checkbox" name="language"> English <br>
            <input type="checkbox" name="language"> Other <br><br>

        <label> Bio:</label> <br> <br>
            <textarea name="Bio" cols="38" Rows="10"> </textarea> <br>
        <input type="submit">
       
    </form>

</body>
</html>