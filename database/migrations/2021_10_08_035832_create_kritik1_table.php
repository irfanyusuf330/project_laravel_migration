<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKritik1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kritik1', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('film_id');
            $table->foreign('film_id')->references('id')->on('film1');
            $table->text('isi');
            $table->integer('point');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kritik1');
        // Schema::table('kritik', function (Blueprint $table) {
        //     $table->dropTimestamps();
        //     $table->dropColomn(['point']);         
        //     $table->dropColomn(['isi']);
        //     $table->dropForeign(['film_id']);
        //     $table->dropForeign(['user_id']);
        //     $table->dropColomn(['id']);          
        // });
    }
}
